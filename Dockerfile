FROM debian:stable
MAINTAINER Marko Dimjašević <marko@dimjasevic.net>

ENV DEBIAN_FRONTEND noninteractive

# Add package sources and Stable backports
RUN echo "deb-src http://httpredir.debian.org/debian stable main" >> /etc/apt/sources.list
RUN echo "deb http://mirrors.kernel.org/debian jessie-backports main" >> /etc/apt/sources.list

RUN apt-get update
RUN apt-get install --yes build-essential
RUN apt-get install --yes emacs sudo git python wget

# Create a sudo user "docker"
ENV HOME_DIR /home/docker
RUN adduser --disabled-password docker
RUN adduser docker sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

WORKDIR ${HOME_DIR}
USER docker

# Install LLVM
# A work-around for the ENV command not doing shell expansion.
# Later prepend a needed RUN command with: bash -c 'source build-env && ...'
RUN echo "export LLVM_VERSION=3.4" >> build-env
RUN bash -c 'source build-env && sudo apt-get install --yes clang-${LLVM_VERSION} llvm-${LLVM_VERSION} llvm-${LLVM_VERSION}-dev llvm-${LLVM_VERSION}-tools'

# Get the Whole Program LLVM tool and configure it
RUN git clone https://github.com/travitch/whole-program-llvm.git
RUN echo "export LLVM_COMPILER=clang" >> build-env
RUN bash -c 'source build-env && echo "export LLVM_COMPILER_PATH=$(llvm-config-${LLVM_VERSION} --prefix)/bin" >> build-env'
RUN bash -c 'echo "export PATH=$PATH:${HOME_DIR}/whole-program-llvm" >> build-env'

# Get and install STP, KLEE and their dependencies
RUN bash -c 'source build-env && sudo apt-get install --yes libboost-program-options1.55.0 libgcc1 libstdc++6 minisat libcap2 libffi6 libllvm${LLVM_VERSION} libtinfo5 python python-tabulate'

ENV STP_PKG  stp_2.1.1+dfsg-1_amd64.deb
ENV KLEE_PKG klee_1.1.0-1_amd64.deb
RUN wget https://dimjasevic.net/marko/klee/${STP_PKG}
RUN wget https://dimjasevic.net/marko/klee/${KLEE_PKG}
RUN sudo dpkg -i ${STP_PKG}
RUN sudo dpkg -i ${KLEE_PKG}

# Pick a target and prepare the environment for building
ENV target pkg-config
RUN sudo apt-get build-dep --yes $target
RUN apt-get source $target

# When running WLLVM, always prepend it with "source build-env && ..."

USER root
# Remove GCC binaries and replace them with symlinks to LLVM
# This snippet was adopted from clang.debian.net
WORKDIR /usr/bin
# ENV GCC_VERSIONS="4.9"
ENV GCC_VERSION="4.9"
# RUN for VERSION in $VERSIONS; do \
RUN rm g++-$GCC_VERSION gcc-$GCC_VERSION cpp-$GCC_VERSION
RUN bash -c 'source /home/docker/build-env && ln -s clang++-${LLVM_VERSION} g++-$GCC_VERSION'
RUN bash -c 'source /home/docker/build-env && ln -s clang-${LLVM_VERSION} gcc-$GCC_VERSION'
RUN bash -c 'source /home/docker/build-env && ln -s clang-${LLVM_VERSION} cpp-$GCC_VERSION'
    # done

RUN echo "Block the installation of new gcc version"
# RUN echo "gcc-5.3 hold"|dpkg --set-selections
# RUN echo "cpp-5.3 hold"|dpkg --set-selections
# RUN echo "g++-5.3 hold"|dpkg --set-selections
RUN echo "gcc-4.9 hold"|dpkg --set-selections
RUN echo "cpp-4.9 hold"|dpkg --set-selections
RUN echo "g++-4.9 hold"|dpkg --set-selections

RUN echo "Check if gcc, g++ & cpp are actually clang"
RUN gcc --version|grep clang > /dev/null || exit 1

# End of the snippet


WORKDIR ${HOME_DIR}
USER docker

WORKDIR pkg-config-0.28
RUN bash -c 'source ${HOME_DIR}/build-env && CC=wllvm CXX=wllvm++ fakeroot debian/rules build'
RUN bash -c 'source ${HOME_DIR}/build-env && CC=wllvm CXX=wllvm++ extract-bc pkg-config'
# The last command produces pkg-config.bc, a single linked LLVM bitcode file
